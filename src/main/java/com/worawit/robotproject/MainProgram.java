/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(2, 2, 'x', map,10);
        Bomb bomb = new Bomb(5,5);
        map.addObj(new Tree(4, 4));
        map.addObj(new Tree(3, 4));
        map.addObj(new Tree(5, 4));
        map.addObj(new Tree(10, 8));
        map.addObj(new Tree(15, 8));
        map.addObj(new Tree(10, 8));
        map.addObj(new Tree(11, 11));
        map.addObj(new Tree(12, 11));
        map.addObj(new Tree(13, 11));
        map.addObj(new Fuel(13, 12,20));
        map.addObj(new Fuel(5, 3,15));
        map.addObj(new Fuel(3, 5,20));
        map.setBomb(bomb);
        map.setRobot(robot);
        while(true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if(direction=='q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
